const express = require("express");
const port = 3000;

let app = express();
let routes = require("express").Router();

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type,authorization"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

routes.post("/api/login", (req, res, next) => {
  res.status(200).json({
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Iml6YUBsaXZlLm5sIiwidXNlcklkIjoiNjA3MDI2M2U5NzNiNjAzZDE4YWM2M2MzIiwiaWF0IjoxNjE4NDE0MzQ1LCJleHAiOjE2MTg0MTc5NDV9.NyxhE_wBp60N47v-x7b_01uh_3tjwsqKoI1ONcxnNWU",
    "expiresIn": 3600000,
    "user": {
        "_id": "6070263e973b603d18ac63c3",
        "firstName": "Iza",
        "lastName": "Moerenhout",
        "email": "iza@live.nl",
        "password": "$2b$10$TSbWAmCNPYwyZXBhhTwFUeF3O5WEDaWqg2vTV3b/1WOYKz7pYkHJG",
        "__v": 0
    }
  })
});

routes.get('api/courses', (req, res, next) => {
  res.status(200).json({
    "result": [
      {
        "_id": "6072efd5d19ddb4bd8bc4c14",
        "courseName": "Programmeren 1",
        "courseCode": "PROG1",
        "credits": 4,
        "semester": "1.1",
        "teacher": {
            "_id": "6071c11091bd525c4c7ea3a7",
            "firstName": "Dion",
            "lastName": "Koeze",
            "email": "d.koeze@avans.nl",
            "__v": 0
        },
        "lessons": [],
        "creator": {
            "_id": "6070263e973b603d18ac63c3",
            "firstName": "Iza",
            "lastName": "Moerenhout",
            "email": "iza@live.nl",
            "password": "$2b$10$TSbWAmCNPYwyZXBhhTwFUeF3O5WEDaWqg2vTV3b/1WOYKz7pYkHJG",
            "__v": 0
        },
        "__v": 18,
        "id": "6072efd5d19ddb4bd8bc4c14"
    },
    ]
  });
})

routes.get("/api/teachers", (req, res, next) => {
  res.status(200).json({
    "result": [
      {
          "_id": "6071bfe5e2b49f0448af8b9f",
          "firstName": "Davide",
          "lastName": "Ambesi",
          "email": "d.ambesi@avans.nl",
          "__v": 0
      },
    ]
  })
});

app.use(routes);

app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});
