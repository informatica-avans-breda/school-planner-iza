import { browser, by, element, logging } from 'protractor';
import { protractor } from 'protractor/built/ptor';
import { CourseEditPage } from './course-edit.po';

describe('Course edit page', () => {
  let page: CourseEditPage;

  beforeEach(() => {
    page = new CourseEditPage();
    browser.waitForAngularEnabled(false);
  });

  it('should display course fields', () => {
    page.navigateTo('/login');
    page.emailUser.sendKeys('iza@live.nl');
    page.passwordInput.sendKeys('test123');

    page.submitButton.click();
    browser.driver.sleep(2000);

    page.navigateTo('/courses/6072efd5d19ddb4bd8bc4c14/edit');
    expect(browser.getCurrentUrl()).toContain('/courses/6072efd5d19ddb4bd8bc4c14/edit');
    expect(page.courseName).toBeTruthy();
    expect(page.courseCode).toBeTruthy();
    expect(page.credits).toBeTruthy();
    expect(page.semester).toBeTruthy();
    expect(page.teacher).toBeTruthy();
  });

  it('should display invalid error when name is not filled in', () => {
    page.navigateTo('/login');
    page.emailUser.sendKeys('iza@live.nl');
    page.passwordInput.sendKeys('test123');

    page.submitButton.click();
    browser.driver.sleep(2000);

    page.navigateTo('/courses/6072efd5d19ddb4bd8bc4c14/edit');
    expect(browser.getCurrentUrl()).toContain('/courses/6072efd5d19ddb4bd8bc4c14/edit');
    browser.driver.sleep(100);
    page.courseName.sendKeys('');
    page.courseName.sendKeys(protractor.Key.BACK_SPACE);
    browser.actions()
    .mouseMove({x: -100, y: 0})
    .click()
    .perform();
    browser.driver.sleep(100);

    expect(page.nameInvalid).toBeTruthy();
    // expect(page.nameInvalid.getText()).toEqual('Naam van het vak is vereist.');
    // expect(page.submitCourse.isEnabled()).toBe(false);
  });

  it('should have button enabled when all fields are filled in', () => {
    page.navigateTo('/login');
    page.emailUser.sendKeys('iza@live.nl');
    page.passwordInput.sendKeys('test123');

    page.submitButton.click();
    browser.driver.sleep(2000);

    page.navigateTo('/courses/6072efd5d19ddb4bd8bc4c14/edit');
    expect(browser.getCurrentUrl()).toContain('/courses/6072efd5d19ddb4bd8bc4c14/edit');
    browser.driver.sleep(100);
    page.courseName.sendKeys('Test');
    page.courseCode.sendKeys('Test');
    page.credits.sendKeys(2);
    page.semester.sendKeys('1.1');
    page.teacher.sendKeys('6071bfe5e2b49f0448af8b9f');

    expect(page.submitCourse.isEnabled()).toBe(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
})
