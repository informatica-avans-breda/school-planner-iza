import { browser, by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../common.po';

export class CourseEditPage extends CommonPageObject {

  get courseName(): ElementFinder {
    return element(by.id('courseName')) as ElementFinder;
  }

  get nameInvalid(): ElementFinder {
    return element(by.id('errorName')) as ElementFinder;
  }

  get courseCode(): ElementFinder {
    return element(by.id('courseCode')) as ElementFinder;
  }

  get credits(): ElementFinder {
    return element(by.id('credits')) as ElementFinder;
  }

  get semester(): ElementFinder {
    return element(by.id('semester')) as ElementFinder;
  }

  get teacher(): ElementFinder {
    return element(by.id('teacher')) as ElementFinder;
  }

  get submitCourse(): ElementFinder {
    return element(by.id('submitCourse')) as ElementFinder;
  }

  get emailUser(): ElementFinder {
    return element(by.id('email')) as ElementFinder;
  }

  get passwordInput(): ElementFinder {
    return element(by.id('password')) as ElementFinder;
  }

  get submitButton(): ElementFinder {
    return element(by.css('form button')) as ElementFinder;
  }
}
