import { browser, by, element, logging } from 'protractor';
import { TeacherListPage } from './teacher-list.po';

describe('Teacher overview', () => {
  let page: TeacherListPage;

  beforeEach(() => {
    page = new TeacherListPage();
    browser.waitForAngularEnabled(false);
  })

  it('should be at /teacher route after initialisation', () => {
    page.navigateTo('/teachers');
    expect(browser.getCurrentUrl()).toContain('/teachers');
  })

  it('should display a list of teachers', () => {
    page.navigateTo('/teachers');

    browser.driver.sleep(500);

    expect(page.emailTeacher).toBeTruthy();
    expect(page.emailTeacher.getText()).toEqual('d.ambesi@avans.nl');
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
})
