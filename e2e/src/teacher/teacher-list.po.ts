import { browser, by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../common.po';

export class TeacherListPage extends CommonPageObject{
  get emailTeacher(): ElementFinder {
    return element(by.id('emailTeacher')) as ElementFinder;
  }
}
