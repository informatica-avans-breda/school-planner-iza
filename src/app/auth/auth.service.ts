import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as Excel from "exceljs/dist/exceljs.min.js";
import { environment } from '../../environments/environment';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { AlertService } from '../shared/alert.service';

import { User } from '../models/user.model';

const BACKEND_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated = false;
  private token: string;
  private tokenTimer;
  authStatusListener$ = new Subject<boolean>();
  private user: User;
  userUpdateListener$ = new Subject<User>();

  constructor(private router: Router, private http: HttpClient, private alertService: AlertService) { }

  autoAuthUser() {
    const authInfo = this.getAuthData();

    if (!authInfo) {
      return;
    }

    const now = new Date();
    const expiresIn = authInfo.expirationDate.getTime() - now.getTime();

    if (expiresIn > 0) {
      this.token = authInfo.token;
      this.isAuthenticated = true;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener$.next(true);
    }
  }

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authStatusListener$.asObservable();
  }

  getUser() {
    return this.user;
  }

  getUserUpdateListener() {
    return this.userUpdateListener$.asObservable();
  }

  createUser(user: User) {
    this.http.post(BACKEND_URL + '/register', user)
      .subscribe(response => {
        console.log(response);
        this.router.navigate(['/']);
        this.alertService.succes('Succesvol geregistreerd!');
      });
  }

  login(emailaddress: string, pass: string) {
    const authData = {
      email: emailaddress,
      password: pass
    };
    this.http.post<{ token: string, expiresIn: number, user: User }>(BACKEND_URL + '/login', authData)
      .subscribe(response => {
        console.log(response);
        const expiresInDuration = response.expiresIn;

        this.token = response.token;
        this.isAuthenticated = true;
        this.authStatusListener$.next(true);
        this.user = response.user;

        const now = new Date();
        const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
        this.saveAuthData(response.token, expirationDate, response.user);

        this.router.navigate(['/']);
      });
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener$.next(false);
    clearTimeout(this.tokenTimer);
    this.clearAuthData();

    this.router.navigate(['/']);
  }

  private setAuthTimer(duration: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(token: string, expirationDate: Date, user: User) {
    localStorage.setItem('token', token);
    localStorage.setItem('expiration', expirationDate.toISOString());
    localStorage.setItem('userFirstName', user.firstName);
    localStorage.setItem('userLastName', user.lastName);
    localStorage.setItem('userId', user._id);
  }

  private clearAuthData() {
    localStorage.removeItem('token');
    localStorage.removeItem('expiration');
    localStorage.removeItem('userFirstName');
    localStorage.removeItem('userLastName');
    localStorage.removeItem('userId');
  }

  private getAuthData() {
    const token = localStorage.getItem('token');
    const expirationDate = localStorage.getItem('expiration');
    const userFirstName = localStorage.getItem('userFirstName');
    const userLastName = localStorage.getItem('userLastName');
    const userId = localStorage.getItem('userId');

    if (!token || !expirationDate) {
      return;
    }

    return {
      token: token,
      expirationDate: new Date(expirationDate),
      userFirstName: userFirstName,
      userLastName: userLastName,
      userId: userId
    }
  }


}
