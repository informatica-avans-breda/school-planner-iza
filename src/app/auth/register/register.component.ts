import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AlertService } from '../../shared/alert.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  isLoading = false;

  constructor(private router: Router, public authService: AuthService, private alertService: AlertService) { }

  ngOnInit(): void {
  }

  onRegister(form: NgForm): void {
    if (form.invalid) {
      return;
    }
    this.authService.createUser(form.value);
  }
}
