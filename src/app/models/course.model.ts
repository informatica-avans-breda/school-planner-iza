import { Lesson } from './lesson.model';
import { Teacher } from './teacher.model';
import { User } from './user.model';

export interface Course {
  id: string;
  courseName: string;
  courseCode: string;
  credits: number;
  semester: string;
  teacher: Teacher;
  lessons: Lesson[];
  creator: User;
}
