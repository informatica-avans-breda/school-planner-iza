export interface Exercise {
  id: string;
  exerciseNumber: number;
  question: string;
  typeOfExercise: string;
  dueDate: Date;
  isDone: boolean;
}
