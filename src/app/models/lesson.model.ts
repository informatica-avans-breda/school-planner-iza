import { Exercise } from "./exercise.model";

export interface Lesson {
  id: string;
  lessonNumber: number;
  date: Date;
  duration: number;
  subject: string;
  exercises: Exercise[];
}
