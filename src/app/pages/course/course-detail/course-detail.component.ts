import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

import { Course } from '../../../models/course.model';
import { CourseService } from '../course.service';

import { Teacher } from '../../../models/teacher.model';
import { TeacherService } from '../../teacher/teacher.service';

import { LessonService } from '../../lesson/lesson.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {
  course: Course;
  courseId: string;
  teacher: Teacher;
  private teacher$ = Subscription;
  isLoading = false;
  private auth$: Subscription;
  userIsAuthenticated: boolean = false;

  constructor(public courseService: CourseService, public lessonService: LessonService, public teacherService: TeacherService, private route: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.isLoading = true;
    this.courseId = this.route.snapshot.paramMap.get('id');

    this.courseService.getById(this.courseId)
      .subscribe((courseData) => {
        this.course = {
          id: courseData._id,
          courseName: courseData.courseName,
          courseCode: courseData.courseCode,
          credits: courseData.credits,
          semester: courseData.semester,
          teacher: courseData.teacher,
          lessons: courseData.lessons,
          creator: courseData.creator
        };
        this.isLoading = false;
      });

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

  }

  onDelete(lessonId: string, courseId: string) {
    this.lessonService.delete(lessonId, courseId);
    console.log(lessonId, ' is verwijderd');
  }

}
