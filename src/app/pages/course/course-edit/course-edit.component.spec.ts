import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BehaviorSubject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from 'src/app/models/course.model';
import { User } from 'src/app/models/user.model';
import { CourseService } from '../course.service';

import { CourseEditComponent } from './course-edit.component';

describe('CourseEditComponent', () => {
  let component: CourseEditComponent;
  let fixture: ComponentFixture<CourseEditComponent>;
  let routerSpy;
  let authServiceSpy;
  let courseServiceSpy;

  const expectedCourse: Course = {
    id: 'id',
    courseName: 'Programmeren 1',
    courseCode: 'PROG1',
    credits: 3,
    semester: '1.1',
    teacher: {
      id: '1',
      firstName: 'Dion',
      lastName: 'Koeze',
      email: 'd.koeze@avans.nl'
    },
    lessons: [],
    creator: {
      _id: '4',
      firstName: 'Iza',
      lastName: 'Moerenhout',
      email: 'iza@live.nl',
      password: 'Secret123'
    }
  }

  const expectedUserData: any = {
    _id: '4',
    firstName: 'Iza',
    lastName: 'Moerenhout',
    email: 'iza@live.nl',
    password: 'Secret123'
  }

  beforeEach(async () => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['createUser', 'login', 'logout'])
    courseServiceSpy = jasmine.createSpyObj('CourseService', ['getAll', 'getById', 'update'])
    const mockUsers$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.userUpdateListener$ = mockUsers$;
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl'])

    await TestBed.configureTestingModule({
      declarations: [ CourseEditComponent ],
      providers: [
        {provide: CourseService, useValue: courseServiceSpy}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CourseEditComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('get course works', () => {
  //   courseServiceSpy.getById.and.returnValue(of(expectedCourse));
  //   fixture.detectChanges();
  //   expect(component.course).toEqual(expectedCourse);
  // });

  it('get course fails', () => {
    courseServiceSpy.getById.and.returnValue(of(undefined));
    fixture.detectChanges();
    expect(component.course).toEqual(undefined);
  });

  // it('put course', () => {
  //   courseServiceSpy.getById.and.returnValue(of(expectedCourse))
  //   courseServiceSpy.update.and.returnValue(of(expectedCourse))
  //   fixture.detectChanges();
  //   component.onSubmit()
  //   expect(courseServiceSpy.update).toHaveBeenCalled()
  // });
});
