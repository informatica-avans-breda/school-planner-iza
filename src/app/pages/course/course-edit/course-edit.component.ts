import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Course } from 'src/app/models/course.model';
import { CourseService } from '../course.service';

import { Teacher } from 'src/app/models/teacher.model';
import { TeacherService } from '../../teacher/teacher.service';

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.css']
})

export class CourseEditComponent implements OnInit {
  id: string;
  title: string;
  course: Course;
  teachers: Teacher [] = [];
  private teachers$: Subscription;
  mode = 'create';
  courseForm: FormGroup;

  semesters: any[] = [ '1.1', '1.2', '1.3', '1.4', '2.1', '2.2', '2.3', '2.4', '3.3', '3.4' ];

  constructor(public courseService: CourseService, private teacherService: TeacherService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.courseForm = this.initForm();
    this.teachers = this.getTeachers();

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.mode = 'edit';
        this.id = paramMap.get('id');

        this.courseService.getById(this.id)
          .subscribe(courseData => {
            this.course = {
              ...courseData,
              id: courseData._id,
            };
            console.log(this.course);
            this.title = courseData.courseName;

            // lessons is a FormArray which contains FormControls
            // we need to retrieve the FormArray and push a new FormControl
            // into it for each lesson
            const lessons = this.courseForm.get('lessons') as FormArray;

            for (const lesson of this.course.lessons) {
              lessons.push(new FormControl(lesson));
            }

            // we're in edit mode, so we have to populate the fields with the course's data
            this.courseForm.patchValue(this.course);
          });
      } else {
        this.mode = 'create';
        this.title = 'Nieuw vak';
        this.id = null;
      }
    });
  }

  onSubmit(): void {
    if (this.courseForm.invalid) {
      console.log('invalid form')
      console.log(this.courseForm)
      return;
    }

    const course: Course = {
      id: null,
      courseName: this.courseForm.value.courseName,
      courseCode: this.courseForm.value.courseCode,
      credits: this.courseForm.value.credits,
      semester: this.courseForm.value.semester,
      teacher: this.courseForm.value.teacher,
      lessons: this.courseForm.value.lessons,
      creator: this.courseForm.value.creator
    };

    if (this.mode === 'create') {
      this.courseService.create(course);
    } else {
      this.courseService.update(this.id, course);
    }

    this.onCancel();
  }

  onCancel(): void {
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private initForm(): FormGroup {
    return new FormGroup({
      courseName: new FormControl(null, { validators: [Validators.required, Validators.minLength(3)] }),
      courseCode: new FormControl(null, { validators: [Validators.required] }),
      credits: new FormControl(null, { validators: [Validators.required] }),
      semester: new FormControl(null, { validators: [Validators.required] }),
      teacher: new FormControl(null , { validators: [Validators.required] }),
      lessons: new FormArray([]),
      creator: new FormControl(localStorage.getItem('userId'), {validators: [Validators.required]}),
    });
  }

  getTeachers(): any {
    this.teacherService.getAll();
    this.teachers$ =  this.teacherService.getTeacherUpdateListener().subscribe((teachers: Teacher[]) => {this.teachers = teachers;});
  }

}
