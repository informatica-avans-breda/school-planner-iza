import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertService } from '../../../shared/alert.service';

import { Course } from '../../../models/course.model';
import { CourseService } from '../course.service';

import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})

export class CourseListComponent implements OnInit, OnDestroy {
  courses: Course[] = [];
  coursesUser: Course[] = [];
  private courses$: Subscription;
  isLoading = false;
  private auth$: Subscription;
  userIsAuthenticated: boolean = false;
  userIsAuthor = false;
  private user$: Subscription;
  user: User;

  constructor(public courseService: CourseService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.isLoading = true;

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

      // if(this.userIsAuthenticated){
        //   this.courseService.getAllByUser(this.user);
        // } else {
          // }

    this.courseService.getAll();
    this.courses$ = this.courseService.getCourseUpdateListener()
      .subscribe((courses: Course[]) => {
        this.isLoading = false;
        this.courses = courses;
      });

    this.user = this.authService.getUser();
    this.user$ = this.authService.getUserUpdateListener()
      .subscribe(user => {
        this.user = user;
      });

  }

  onDelete(courseid: string): void{
    this.courseService.delete(courseid);
    console.log(courseid, ' is verwijderd');
  }

  ngOnDestroy(): void {
    this.courses$.unsubscribe();
  }
}
