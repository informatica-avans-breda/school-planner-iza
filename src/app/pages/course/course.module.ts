import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { CourseEditComponent } from './course-edit/course-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: CourseListComponent },
  { path: 'add', pathMatch: 'full', component: CourseEditComponent },
  { path: ':id/edit', pathMatch: 'full', component: CourseEditComponent },
  { path: ':id', pathMatch: 'full', component: CourseDetailComponent },
  { path: ':id/lesson', loadChildren: () => import('../lesson/lesson.module').then(m => m.LessonModule)}
];

@NgModule({
  declarations: [CourseListComponent, CourseDetailComponent, CourseEditComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule
  ]
})
export class CourseModule { }
