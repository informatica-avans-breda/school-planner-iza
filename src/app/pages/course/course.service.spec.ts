import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Course } from 'src/app/models/course.model';

import { CourseService } from './course.service';

describe('CourseService', () => {
  let service: CourseService;
  let httpClientSpy;
  let courseServiceSpy;

  const expectedCourse: Course[] = [{
    id: '345',
    courseName: 'Programmeren 1',
    courseCode: 'PROG1',
    credits: 3,
    semester: '1.1',
    teacher: {
      id: '1',
      firstName: 'Dion',
      lastName: 'Koeze',
      email: 'd.koeze@avans.nl'
    },
    lessons: [],
    creator: {
      _id: '4',
      firstName: 'Iza',
      lastName: 'Moerenhout',
      email: 'iza@live.nl',
      password: 'Secret123'
    }
  }];

  beforeEach( () => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
    courseServiceSpy = jasmine.createSpyObj('CourseService', ['create', 'getAll', 'getById', 'update', 'delete', 'getCourseUpdateListener']);

    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });

    service =  TestBed.inject(CourseService);
    // httpClientSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all courses', () => {
    httpClientSpy.get.and.returnValue(of(expectedCourse));

    service.getAll();
    const sub = service.getCourseUpdateListener()
      .subscribe((courses) => {
        expect(courses).toEqual(expectedCourse)
    })
    sub.unsubscribe();
  });

  // it('should by id', () => {
  //   httpClientSpy.get.and.returnValue(of(expectedCourse));

  //   const sub = service.getById("345").subscribe((courses) => {
  //       expect(courses).toEqual(expectedCourse[0])
  //   });
  //   sub.unsubscribe();
  // });

  it('should create a course', () => {
    httpClientSpy.post.and.returnValue(of(expectedCourse[0]));

    service.create(expectedCourse[0]);
    const sub = service.getCourseUpdateListener().subscribe((course) => {
      expect(course).toEqual(expectedCourse);
    })
    sub.unsubscribe()
  });

  it('should update a course', () => {
    httpClientSpy.put.and.returnValue(of(expectedCourse[0]));

    service.update("345", expectedCourse[0]);
    const sub = service.getCourseUpdateListener().subscribe((course) => {
      expect(course).toEqual(expectedCourse);
    })
    sub.unsubscribe()
  });

  // it('should delete a course', () => {
  //   httpClientSpy.delete.and.returnValue(of(expectedCourse[0]));

  //   service.delete("345");
  //   const sub = service.getCourseUpdateListener().subscribe((course) => {
  //     expect(course).toEqual(expectedCourse);
  //   })
  //   sub.unsubscribe()
  // });

  // it('should fail create a course', () => {
  //   const expectedErrorResponse = {
  //     error: { message: 'Something went wrong' },
  //     name: 'HttpErrorResponse',
  //     ok: false,
  //     status: 404,
  //   };

  //   httpClientSpy.post.and.returnValue(throwError(expectedErrorResponse));

  //   service.create(expectedCourse[0]);
  //   const sub = service.getCourseUpdateListener().subscribe((course) => {
  //     expect(course).toEqual(undefined);
  //   })
  //   sub.unsubscribe()
  // });
});
