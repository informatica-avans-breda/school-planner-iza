import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Course } from '../../models/course.model';

import { environment } from '../../../environments/environment';
import { Lesson } from 'src/app/models/lesson.model';
import { User } from 'src/app/models/user.model';
import { Teacher } from 'src/app/models/teacher.model';

const BACKEND_URL = environment.apiUrl + '/courses/';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private courses: Course[] = [];
  private coursesUpdated = new Subject<Course[]>();

  constructor(private http: HttpClient) { }

  getCourseUpdateListener(): Observable<Course[]>{
    return this.coursesUpdated.asObservable();
  }

  create(newCourse: Course){
    console.log(newCourse);
    return this.http
    .post<{ id: string }>(BACKEND_URL, newCourse)
    .subscribe(responseData => {
      newCourse.id = responseData.id;
      this.courses.push(newCourse);
      this.coursesUpdated.next([...this.courses]);
    });
  }

  getAll() {
    this.http
    .get<{ result: any }>(
      BACKEND_URL
      )
    .pipe(map((courseData) => {
      console.log(courseData);
      return courseData.result.map(course => {
        return {
          courseName: course.courseName,
          courseCode: course.courseCode,
          credits: course.credits,
          semester: course.semester,
          teacher: course.teacher,
          lessons: course.lessons,
          creator: course.creator,
          id: course._id
        };
      });
    }))
    .subscribe(transformedCourses => {
      this.courses = transformedCourses;
      this.courses = this.courses.filter(course => course.creator._id == localStorage.getItem('userId'));
      this.coursesUpdated.next([...this.courses]);
    });
  }

  getCoursesByUser(userId: string) {
    this.http
    .get<{ result: any }>(
      BACKEND_URL
      )
    .pipe(map((courseData) => {
      console.log(courseData);
      return courseData.result.map(course => {
        return {
          courseName: course.courseName,
          courseCode: course.courseCode,
          credits: course.credits,
          semester: course.semester,
          teacher: course.teacher,
          lessons: course.lessons,
          creator: course.creator,
          id: course._id
        };
      });
    }))
    .subscribe(transformedCourses => {
      this.courses = transformedCourses;
      this.courses = this.courses.filter(course => course.creator._id == userId);
      this.coursesUpdated.next([...this.courses]);
    });
  }

  getById(id: string){
    return this.http.get<{
      _id: string,
      courseName: string,
      courseCode: string,
      credits: number,
      semester: string,
      teacher: Teacher,
      lessons: Lesson[],
      creator: User
    }>(BACKEND_URL + id);
  }

  update(id: string, updateCourse: Course) {
    this.http.put(BACKEND_URL + id, updateCourse).subscribe(() => {
      this.courses.push(updateCourse);
      this.coursesUpdated.next([...this.courses]);
    });
  }

  delete(id: string){
    this.http.delete(BACKEND_URL + id).subscribe(() => {
      const updatedCourses = this.courses.filter(course => course.id !== id);
      this.courses = updatedCourses;
      this.coursesUpdated.next([...this.courses]);
    });
  }

}
