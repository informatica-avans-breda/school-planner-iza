import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Exercise } from 'src/app/models/exercise.model';
import { ExerciseEditComponent } from '../exercise-edit/exercise-edit.component';
import { ExerciseService } from '../exercise.service';

@Component({
  selector: 'app-exercise-detail',
  templateUrl: './exercise-detail.component.html',
  styleUrls: ['./exercise-detail.component.css']
})
export class ExerciseDetailComponent implements OnInit {
  exercise: Exercise;
  courseId: string;
  lessonId: string;
  exerciseId: string;
  isLoading = false;
  private auth$: Subscription;
  userIsAuthenticated: boolean = false;

  constructor(public exerciseService: ExerciseService, private route: ActivatedRoute, private authService: AuthService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.courseId = this.route.snapshot.paramMap.get('id');
    this.lessonId = this.route.snapshot.paramMap.get('lessonId');
    this.exerciseId = this.route.snapshot.paramMap.get('exerciseId');

    this.exerciseService.getById(this.exerciseId, this.lessonId, this.courseId)
      .subscribe((exerciseData) => {
        this.exercise = {
          id: exerciseData._id,
          exerciseNumber: exerciseData.exerciseNumber,
          question: exerciseData.question,
          typeOfExercise: exerciseData.typeOfExercise,
          dueDate: exerciseData.dueDate,
          isDone: exerciseData.isDone
        };
        this.isLoading = false;
      });

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });
  }

}
