import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import { Exercise } from 'src/app/models/exercise.model';
import { ExerciseService } from '../exercise.service';

@Component({
  selector: 'app-exercise-edit',
  templateUrl: './exercise-edit.component.html',
  styleUrls: ['./exercise-edit.component.css']
})
export class ExerciseEditComponent implements OnInit {
  lessonId: string;
  courseId: string;
  exerciseId: string;
  title: string;
  exercise: Exercise;
  mode = 'create';
  exerciseForm: FormGroup;

  typeOfExercises: any[] = ['Open vraag', 'Meerkeuze vraag', 'Verslag', 'Programeeropgave']

  constructor(public exeriseService: ExerciseService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.exerciseForm = this.initForm();
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.courseId = paramMap.get('id');
      this.lessonId = paramMap.get('lessonId');
    })

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('exerciseId')) {
        this.mode = 'edit';
        this.exerciseId = paramMap.get('exerciseId');

        this.exeriseService.getById(this.exerciseId, this.lessonId, this.courseId)
          .subscribe(exerciseData => {
            this.exercise = {
              id: exerciseData._id,
              exerciseNumber: exerciseData.exerciseNumber,
              question: exerciseData.question,
              typeOfExercise: exerciseData.typeOfExercise,
              dueDate: exerciseData.dueDate,
              isDone: exerciseData.isDone
            };
            console.log(this.exercise);
            this.title = 'Opdracht' + exerciseData.exerciseNumber

            this.exerciseForm.patchValue({
              exerciseNumber: this.exercise.exerciseNumber,
              question: this.exercise.question,
              typeOfExercise: this.exercise.typeOfExercise,
              dueDate: this.exercise.dueDate,
              isDone: this.exercise.isDone
            })
          });
      } else {
        this.mode = 'create';
        this.title = 'Nieuwe opdracht';
        this.exerciseId = null;
      }
    });
  }

  onSubmit() {
    if (this.exerciseForm.invalid) {
      console.log('invalid form')
      console.log(this.exerciseForm)
      return;
    }

    const exercise: Exercise = {
      id: null,
      exerciseNumber: this.exerciseForm.value.exerciseNumber,
      question: this.exerciseForm.value.question,
      typeOfExercise: this.exerciseForm.value.typeOfExercise,
      dueDate: this.exerciseForm.value.dueDate,
      isDone: this.exerciseForm.value.isDone
    }

    if(this.mode === 'create') {
      this.exeriseService.create(exercise, this.courseId, this.lessonId);
      this.router.navigate(['../../'], {relativeTo: this.route});
    } else {
      this.exeriseService.update(this.exerciseId, exercise, this.lessonId, this.courseId);
      this.router.navigate(['../../../'], {relativeTo: this.route});
    }
  }

  onCancel(): void {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  private initForm(): FormGroup {
    return new FormGroup({
      exerciseNumber: new FormControl(null, { validators: [Validators.required] }),
      question: new FormControl(null, { validators: [Validators.required] }),
      typeOfExercise: new FormControl(null, { validators: [Validators.required] }),
      dueDate: new FormControl(null, { validators: [Validators.required] }),
      isDone: new FormControl(null),
    });
  }

  validNumber(control: FormControl): { [s: string]: number} {
    if(control.value > 0){
      return {invalidNumber: null};
    }
    return {invalidNumber: control.value};
  }
}
