import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExerciseEditComponent } from './exercise-edit/exercise-edit.component';
import { ExerciseDetailComponent } from './exercise-detail/exercise-detail.component';

const routes: Routes = [
  { path: 'add', pathMatch: 'full', component: ExerciseEditComponent },
  { path: ':exerciseId/edit', pathMatch: 'full', component: ExerciseEditComponent },
  { path: ':exerciseId', pathMatch: 'full', component: ExerciseDetailComponent }
]

@NgModule({
  declarations: [ ExerciseDetailComponent, ExerciseEditComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule
  ]
})
export class ExerciseModule { }
