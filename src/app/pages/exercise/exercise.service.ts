import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Exercise } from 'src/app/models/exercise.model';

import { environment } from '../../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/courses/';

@Injectable({
  providedIn: 'root'
})
export class ExerciseService {
  private exercises: Exercise[] = [];
  private exercisesUpdated = new Subject<Exercise[]>();

  constructor(private http: HttpClient) { }

  getExerciseUpdateListener(): Observable<Exercise[]> {
    return this.exercisesUpdated.asObservable();
  }

  create(newExercise: Exercise, courseId: string, lessonId: string) {
    console.log(newExercise);

    return this.http
      .post<{ exerciseId: string }>(BACKEND_URL + courseId + '/lessons/' + lessonId + '/exercises', newExercise)
      .subscribe(responseData => {
        newExercise.id = responseData.exerciseId;
        this.exercises.push(newExercise);
        this.exercisesUpdated.next([...this.exercises]);
      });
  }

  getById(exerciseId: string, lessonId: string, courseId: string) {
    return this.http.get<{
      _id: string,
      exerciseNumber: number,
      dueDate: Date,
      question: string,
      typeOfExercise: string,
      isDone: boolean
    }>(BACKEND_URL + courseId + '/lessons/' + lessonId + '/exercises/' + exerciseId);
  }

  update(id: string, updateExercise: Exercise, lessonId: string, courseId: string) {
    this.http.put(BACKEND_URL + courseId + '/lessons/' + lessonId + '/exercises/' + id, updateExercise).subscribe(() => {
      this.exercises.push(updateExercise);
      this.exercisesUpdated.next([...this.exercises]);
    });
  }

  delete(id: string, lessonId: string, courseId: string){
    this.http.delete(BACKEND_URL + courseId + '/lessons/' + lessonId + '/exercises/' + id).subscribe(() => {
      const updatedExercises = this.exercises.filter(exercise => exercise.id !== id);
      this.exercises = updatedExercises;
      this.exercisesUpdated.next([...this.exercises]);
    });
  }
}
