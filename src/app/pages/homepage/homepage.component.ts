import { Component, OnDestroy, OnInit } from '@angular/core';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
  providers: [NgbCarouselConfig]
})
export class HomepageComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  private auth$: Subscription;
  images = ['./../../../assets/images/avans-at-night.jpg'];

  constructor(config: NgbCarouselConfig, private authService: AuthService) {
    config.showNavigationArrows = false;
    config.showNavigationIndicators = false;
   }

  ngOnInit(): void {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });
  }

  ngOnDestroy() {
    this.auth$.unsubscribe();
  }
}
