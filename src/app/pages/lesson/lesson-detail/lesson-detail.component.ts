import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Lesson } from 'src/app/models/lesson.model';
import { ExerciseService } from '../../exercise/exercise.service';
import { LessonService } from '../lesson.service';

@Component({
  selector: 'app-lesson-detail',
  templateUrl: './lesson-detail.component.html',
  styleUrls: ['./lesson-detail.component.css']
})
export class LessonDetailComponent implements OnInit {
  lesson: Lesson;
  courseId: string;
  lessonId: string;
  isLoading = false;
  private auth$: Subscription;
  userIsAuthenticated: boolean = false;

  constructor(public exerciseService: ExerciseService, public lessonService: LessonService, private route: ActivatedRoute, private authService: AuthService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.courseId = this.route.snapshot.paramMap.get('id');
    this.lessonId = this.route.snapshot.paramMap.get('lessonId');

    this.lessonService.getById(this.lessonId, this.courseId)
      .subscribe((lessonData) => {
        this.lesson = {
          id: lessonData._id,
          lessonNumber: lessonData.lessonNumber,
          duration: lessonData.duration,
          date: lessonData.date,
          subject: lessonData.subject,
          exercises: lessonData.exercises
        };
        this.isLoading = false;
      });

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });
  }

  onDelete(exerciseId: string ,lessonId: string, courseId: string) {
    this.exerciseService.delete(exerciseId, lessonId, courseId);
    console.log(exerciseId, ' is verwijderd');
  }

}
