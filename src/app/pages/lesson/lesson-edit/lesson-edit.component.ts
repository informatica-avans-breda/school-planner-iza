import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import { Subscription } from 'rxjs';

import { Course } from '../../../models/course.model';
import { CourseService } from '../../course/course.service';

import { Lesson } from '../../../models/lesson.model';
import { LessonService } from '../lesson.service';


@Component({
  selector: 'app-lesson-edit',
  templateUrl: './lesson-edit.component.html',
  styleUrls: ['./lesson-edit.component.css']
})
export class LessonEditComponent implements OnInit {
  lessonId: string;
  courseId: string;
  title: string;
  lesson: Lesson;
  mode = 'create';
  lessonForm: FormGroup;

  constructor(public courseService: CourseService, private lessonService: LessonService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.lessonForm = this.initForm();
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.courseId = paramMap.get('id');
    })

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('lessonId')){
        this.mode = 'edit';
        this.lessonId = paramMap.get('lessonId');

        this.lessonService.getById(this.lessonId, this.courseId)
          .subscribe(lessonData => {
            this.lesson = {
              id: lessonData._id,
              lessonNumber: lessonData.lessonNumber,
              duration: lessonData.duration,
              date: lessonData.date,
              subject: lessonData.subject,
              exercises: lessonData.exercises
            };
            console.log(this.lesson);
            this.title = lessonData.lessonNumber + ' ' + lessonData.subject;

            const exercises = this.lessonForm.get('exercises') as FormArray;
            for(const exercise of this.lesson.exercises) {
              exercises.push(new FormControl(exercise));
            }

            this.lessonForm.patchValue({
              lessonNumber: this.lesson.lessonNumber,
              duration: this.lesson.duration,
              date: this.lesson.date,
              subject: this.lesson.subject
            });
          });
      } else {
        this.mode = 'create';
        this.title = 'Nieuwe les';
        this.lessonId = null;
      }
    });
  }

  onSubmit(): void {
    if (this.lessonForm.invalid) {
      console.log('invalid form')
      console.log(this.lessonForm)
      return;
    }

    const lesson: Lesson = {
      id: null,
      lessonNumber: this.lessonForm.value.lessonNumber,
      date: this.lessonForm.value.date,
      duration: this.lessonForm.value.duration,
      subject: this.lessonForm.value.subject,
      exercises: this.lessonForm.value.exercises
    };

    if (this.mode === 'create') {
      this.lessonService.create(lesson, this.courseId);
      this.router.navigate(['../../'], {relativeTo: this.route});
    } else {
      this.lessonService.update(this.lessonId, lesson, this.courseId);
      this.router.navigate(['../../../'], {relativeTo: this.route});
    }

    // this.onCancel();
  }

  onCancel(): void {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  private initForm(): FormGroup {
    return new FormGroup({
      lessonNumber: new FormControl(null, { validators: [Validators.required] }),
      date: new FormControl(null, { validators: [Validators.required] }),
      duration: new FormControl(null, { validators: [Validators.required] }),
      exercises: new FormArray([]),
      subject: new FormControl(null, { validators: [Validators.required] }),
    });
  }
}
