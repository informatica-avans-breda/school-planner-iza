import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LessonDetailComponent } from './lesson-detail/lesson-detail.component';
import { LessonEditComponent } from './lesson-edit/lesson-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExerciseDetailComponent } from '../exercise/exercise-detail/exercise-detail.component';

const routes: Routes = [
  { path: 'add', pathMatch: 'full', component: LessonEditComponent},
  { path: ':lessonId/edit', pathMatch: 'full', component: LessonEditComponent},
  { path: ':lessonId', pathMatch: 'full', component:  LessonDetailComponent},
  { path: ':lessonId/exercise', loadChildren: () => import('../exercise/exercise.module').then(m => m.ExerciseModule)}
]

@NgModule({
  declarations: [ LessonDetailComponent, LessonEditComponent ],
  imports: [
    CommonModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule
  ]
})
export class LessonModule { }
