import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { Course } from 'src/app/models/course.model';
import { Lesson } from 'src/app/models/lesson.model';
import { User } from 'src/app/models/user.model';
import { Exercise } from 'src/app/models/exercise.model';

const BACKEND_URL = environment.apiUrl + '/courses/';

@Injectable({
  providedIn: 'root'
})
export class LessonService {
  private lessons: Lesson[] = [];
  private lessonsUpdated = new Subject<Lesson[]>();

  constructor(private http: HttpClient) { }

  getLessonUpdateListener(): Observable<Lesson[]> {
    return this.lessonsUpdated.asObservable();
  }

  create(newLesson: Lesson, courseId: string) {
    console.log(newLesson);

    return this.http
      .post<{ lessonId: string }>(BACKEND_URL + courseId + '/lessons', newLesson)
      .subscribe(responseData => {
        newLesson.id = responseData.lessonId;
        this.lessons.push(newLesson);
        this.lessonsUpdated.next([...this.lessons]);
      });
  }

  getById(lessonId: string, courseId: string) {
    return this.http.get<{
      _id: string,
      lessonNumber: number,
      date: Date,
      duration: number,
      subject: string,
      exercises: Exercise[]
    }>(BACKEND_URL + courseId + '/lessons/' + lessonId);
  }

  update(id: string, updateLesson: Lesson, courseId: string) {
    this.http.put(BACKEND_URL + courseId + '/lessons/' + id, updateLesson).subscribe(() => {
      this.lessons.push(updateLesson);
      this.lessonsUpdated.next([...this.lessons]);
    });
  }

  delete(id: string, courseId: string){
    this.http.delete(BACKEND_URL + courseId + '/lessons/' + id).subscribe(() => {
      const updatedLessons = this.lessons.filter(lesson => lesson.id !== id);
      this.lessons = updatedLessons;
      this.lessonsUpdated.next([...this.lessons]);
    });
  }
}
