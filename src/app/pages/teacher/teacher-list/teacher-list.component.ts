import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Teacher } from '../../../models/teacher.model';
import { TeacherService } from '../teacher.service';

import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../../models/user.model';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit, OnDestroy {
  teachers: Teacher[] = [];
  private teachers$: Subscription;
  isLoading = false;
  private auth$: Subscription;
  userIsAuthenticated: boolean = false;
  private user$: Subscription;
  user: User;

  constructor(public teacherService: TeacherService,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.teacherService.getAll();
    this.teachers$ = this.teacherService.getTeacherUpdateListener()
      .subscribe((teachers: Teacher[]) => {
        this.isLoading = false;
        this.teachers = teachers;
      });

      this.userIsAuthenticated = this.authService.getIsAuth();
      this.auth$ = this.authService.getAuthStatusListener()
        .subscribe(isAuthenticated => {
          this.userIsAuthenticated = isAuthenticated;
        });

      this.user = this.authService.getUser();
      this.user$ = this.authService.getUserUpdateListener()
        .subscribe(user => {
          this.user = user;
        })
  }

  ngOnDestroy(): void {
    this.teachers$.unsubscribe();
  }

}
