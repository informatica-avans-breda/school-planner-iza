import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TeacherListComponent } from './teacher-list/teacher-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: TeacherListComponent },
]

@NgModule({
  declarations: [TeacherListComponent],
  imports: [ CommonModule, RouterModule.forChild(routes), ReactiveFormsModule, FormsModule ]
})

export class TeacherModule {}
