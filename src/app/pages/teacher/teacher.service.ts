import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Teacher } from '../../models/teacher.model';

import { environment } from '../../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/teachers/';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private teachers: Teacher[] = [];
  private teachersUpdated = new Subject<Teacher[]>();

  constructor(private http: HttpClient) { }

  getTeacherUpdateListener(): Observable<Teacher[]>{
    return this.teachersUpdated.asObservable();
  }

  create(newTeacher: Teacher){
    console.log(newTeacher);
    return this.http
    .post<{ id: string }>(BACKEND_URL, newTeacher)
    .subscribe(responseData => {
      newTeacher.id = responseData.id;
      this.teachers.push(newTeacher);
      this.teachersUpdated.next([...this.teachers]);
    });
  }

  getAll() {
    this.http
    .get<{ result: any }>(
      BACKEND_URL
      )
    .pipe(map((teacherData: any) => {
      console.log(teacherData);
      return teacherData.result.map(teacher => {
        return {
          firstName: teacher.firstName,
          lastName: teacher.lastName,
          email: teacher.email,
          id: teacher._id
        };
      });
    }))
    .subscribe(transformedTeachers => {
      this.teachers = transformedTeachers;
      this.teachersUpdated.next([...this.teachers]);
    });
  }

  getById(id: string){
    return this.http.get<{
      _id: string,
      firstName: string,
      lastname: string,
      email: string
    }>(BACKEND_URL + id);
  }

  update(id: string, updateTeacher: Teacher) {
    this.http.put(BACKEND_URL + id, updateTeacher).subscribe(() => {
      this.teachers.push(updateTeacher);
      this.teachersUpdated.next([...this.teachers]);
    });
  }

  delete(id: string){
    this.http.delete(BACKEND_URL + id).subscribe(() => {
      const updatedTeachers = this.teachers.filter(teacher => teacher.id !== id);
      this.teachers = updatedTeachers;
      this.teachersUpdated.next([...this.teachers]);
    });
  }
}
