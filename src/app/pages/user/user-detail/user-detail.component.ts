import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Course } from 'src/app/models/course.model';
import { UserProfile } from 'src/app/models/user-profile.model';
import { CourseService } from '../../course/course.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  detailUserId: string;
  user: UserProfile;
  private user$: Subscription;
  isLoading = false;
  courses: Course[] = [];
  private courses$: Subscription;
  userIsAuthenticated: boolean = false;
  private auth$: Subscription;

  constructor(private userService: UserService, private route: ActivatedRoute, private authService: AuthService, private courseService: CourseService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.detailUserId = this.route.snapshot.paramMap.get('id');

    this.userService.getById(this.detailUserId)
      .subscribe((userProfile) => {
      this.user = {
        id: userProfile?._id,
        firstName: userProfile?.firstName,
        lastName: userProfile?.lastName,
        email: userProfile?.email
      };
    });

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.courseService.getCoursesByUser(this.detailUserId);
    this.courses$ = this.courseService.getCourseUpdateListener()
      .subscribe((courses: Course[]) => {
        this.isLoading = false;
        this.courses = courses;
      });
  }

}
