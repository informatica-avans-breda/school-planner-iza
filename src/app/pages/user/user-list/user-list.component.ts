import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserProfile } from 'src/app/models/user-profile.model';
import { User } from 'src/app/models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  users: UserProfile[] = [];
  private users$: Subscription;
  isLoading = false;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.userService.getAll();
    this.users$ = this.userService.getUserUpdateListener()
      .subscribe((userProfiles: UserProfile[]) => {
        this.isLoading = false;
        this.users = userProfiles;
      });
  }

  ngOnDestroy(): void {
    this.users$.unsubscribe();
  }

}
