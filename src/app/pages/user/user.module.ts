import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: UserListComponent },
  { path: ':id', pathMatch: 'full', component: UserDetailComponent }
];

@NgModule({
  declarations: [UserListComponent, UserDetailComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class UserModule { }
