import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserProfile } from 'src/app/models/user-profile.model';

import { environment } from '../../../environments/environment';

const BACKEND_URL = environment.apiUrl + '/users/';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userProfiles: UserProfile[] = [];
  private userProfilesUpdated = new Subject<UserProfile[]>();

  constructor(private http: HttpClient) { }

  getUserUpdateListener() {
    return this.userProfilesUpdated.asObservable();
  }

  getAll(){
    this.http.get<{ result: any }>(BACKEND_URL)
      .pipe(map((userData) => {
        return userData.result.map(user => {
          return {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            id: user._id
          };
        });
    }))
    .subscribe(transformedUsers => {
      this.userProfiles = transformedUsers;
      this.userProfiles = this.userProfiles.filter(user => user.id !== localStorage.getItem('userId'));
      this.userProfilesUpdated.next([...this.userProfiles]);
    });
  }

  getById(id: string) {
    return this.http.get<{
      _id: string,
      firstName: string,
      lastName: string,
      email: string
    }>(BACKEND_URL + id);
  }
}
