import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Alert, AlertService } from '../alert.service';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  subs: Subscription;
  alert: Alert;
  staticAlertClosed = true;

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.subs = this.alertService.alert$.subscribe((alert) => {
      this.alert = alert;
      this.staticAlertClosed = false;

      setTimeout(() => (this.staticAlertClosed = true), 5000);
    })

    // this.subs = this.alertService.alert$.subscribe((alert) => {
    //   this.alert = alert;
    //   this.alertMessage = alert.message;
    //   this.staticAlertClosed = false;
    //   setTimeout(() => (this.staticAlertClosed = true), 6000);
    // })

    // this.subs.subscribe(message => {
    //   message = this.alert.message;
    //   this.alertMessage = message;
    // });
    // this.subs.pipe(debounceTime(6000)).subscribe(() => {
    //   if(this.staticAlert){
    //     this.staticAlert.close();
    //   }
    // })
  }

  ngOnDestory(): void {
    this.subs.unsubscribe();
  }

}
