import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  isNavbarCollapsed = true;
  private auth$: Subscription;
  userIsAuthenticated: boolean = false;
  private user$: Subscription;
  user: User;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.auth$ = this.authService.getAuthStatusListener()
      .subscribe(isAuthenticated => {
        this.userIsAuthenticated = isAuthenticated;
      });

    this.user = this.authService.getUser();
    this.user$ = this.authService.getUserUpdateListener()
      .subscribe(user => {
        this.user = user;
      })

  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.auth$.unsubscribe();
    this.user$.unsubscribe();
  }

}
