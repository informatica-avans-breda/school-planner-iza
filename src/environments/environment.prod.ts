export const environment = {
  production: true,
  apiUrl: 'https://school-planner-api.herokuapp.com/api'
};
